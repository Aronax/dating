﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Dating.Common.Extentions
{
	public static class StringExtension
	{
		public static bool IsEmpty(this String str)
		{
			return string.IsNullOrWhiteSpace(str);
		}

		public static bool IsNotEmpty(this String str)
		{
			return string.IsNullOrWhiteSpace(str) == false;
		}

		public static string ToCamel(this String str)
		{
			if (str.IsEmpty())
				return str;

			var split = Regex.Replace(str, "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1 ");

			split = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(split.ToLower());
			split = split.Replace(" ", "");
			return split;
		}

		public static string ToTitle(this String str)
		{
			return str.IsEmpty()
						  ? str
						  : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
		}

		public static string Fmt(this string str, params object[] args)
		{
			return string.Format(str, args);
		}

		public static string Fmt(this string str, IFormatProvider provider, params object[] args)
		{
			return string.Format(provider, str, args);
		}

		public static bool Like(this string toSearch, string toFind)
		{
			var result = new Regex(@"\A" + new Regex(@"\.|\$|\^|\{|\[|\(|\||\)|\*|\+|\?|\\").Replace(toFind, ch => @"\" + ch)
																													  .Replace('_', '.')
																													  .Replace("%", ".*") + @"\z", RegexOptions.Singleline).IsMatch(toSearch);
			return result;
		}

	}
}
