﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Dating.Common.Extentions
{
	public static class Collections
	{
		public static List<List<int>> Split(this List<int> list, int width)
		{
			var result = new List<List<int>>();

			var numberOfLists = (list.Count / width) + 1;

			for (var i = 0; i < numberOfLists; i++)
			{
				result.Add(list.Skip(i * width)
									.Take(width)
									.ToList());
			}

			return result;
		}

		public static void AddRange<T>(this BindingList<T> collection, IEnumerable<T> items)
		{
			if (collection == null)
				return;

			if (items == null)
				return;

			foreach (var item in items)
			{
				collection.Add(item);
			}
		}

		public static bool IsEmpty<T>(this IEnumerable<T> list)
		{
			return list == null || list.Any() == false;
		}

		public static bool IsNotEmpty<T>(this IEnumerable<T> list)
		{
			return !IsEmpty(list);
		}


		public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
		{
			if (collection == null)
				return;

			if (items == null)
				return;

			foreach (var item in items)
			{
				collection.Add(item);
			}
		}

		public static void SortBy<T>(this ObservableCollection<T> collection, string order)
		{
			var prop = typeof(T).GetProperty(order);
			if (prop == null)
				return;

			var items = collection.OrderBy(i => prop.GetValue(i))
										 .ToList();
			collection.Clear();
			collection.AddRange(items);
		}

		public static void Add<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict, IEnumerable<TValue> list, Func<TValue, TKey> keySelector)
		{
			if (dict == null)
				return;

			foreach (var value in list)
			{
				dict.AddOrUpdate(keySelector(value), value, (key, val) => value);
			}
		}

		public static void AddFromList<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict, IEnumerable<TValue> list, Func<TValue, TKey> keySelector)
		{
			if (dict == null)
				return;

			foreach (var value in list)
			{
				dict.AddOrUpdate(keySelector(value), value, (key, val) => value);
			}
		}

		public static void Update<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict, TKey key, Action<TValue> action)
		{
			if (dict == null)
				return;

			TValue value;
			if (dict.TryGetValue(key, out value))
				action(value);
		}
	}
}
