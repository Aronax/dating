﻿using System;
using System.CodeDom;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.CSharp;
using Newtonsoft.Json;

namespace Dating.Common.Extentions
{
	public static class MainExtentions
	{
		private static readonly ConcurrentDictionary<Type, List<PropertyInfo>> PropertyDictionary =
			new ConcurrentDictionary<Type, List<PropertyInfo>>();

		public static IEnumerable<PropertyInfo> GetProperties(Type type)
		{
			List<PropertyInfo> properties;
			if (PropertyDictionary.TryGetValue(type, out properties))
			{
				return properties;
			}

			properties = type.GetProperties()
			                 .ToList();
			PropertyDictionary.TryAdd(type, properties);

			return properties;
		}

		public static bool In<T>(this T source,
		                         params T[] list)
		{
			return source != null && list.Contains(source);
		}

		public static bool NotIn<T>(this T source,
		                            params T[] list)
		{
			return (source != null && list.Contains(source)) == false;
		}

		public static bool Between<T>(this T actual,
		                              T lower,
		                              T upper) where T : IComparable<T>
		{
			return actual.CompareTo(lower) >= 0 && actual.CompareTo(upper) < 0;
		}

		public static void ForEach<T>(this IEnumerable<T> source,
		                              Action<T> action)
		{
			foreach (var element in source)
			{
				action(element);
			}
		}

		public static bool EqualsAll<T>(this IList<T> list)
		{
			if (list == null || list.Count < 2)
			{
				return true;
			}
			var first = list[0];
			for (var i = 1; i < list.Count; i++)
			{
				if (!first.Equals(list[i]))
				{
					return false;
				}
			}
			return true;
		}

		public static bool EqualsAll<T>(params T[] list)
		{
			if (list == null || list.Length < 2)
			{
				return true;
			}
			var first = list[0];
			for (var i = 1; i < list.Length; i++)
			{
				if (!first.Equals(list[i]))
				{
					return false;
				}
			}
			return true;
		}

		public static decimal GetMedian(this IEnumerable<int> source)
		{
			if (source == null)
			{
				return 0;
			}

			var temp = source.ToArray();
			Array.Sort(temp);

			if (temp.Length == 0)
			{
				return 0;
			}

			if (temp.Length%2 == 0)
			{
				return (temp[temp.Length/2 - 1] + temp[temp.Length/2])/2m;
			}
			return temp[temp.Length/2];
		}

		public static T Merge<T>(this T source,
		                         T newValue) where T : class
		{
			if (source == null)
			{
				return null;
			}

			if (newValue == null)
			{
				return source;
			}

			foreach (var info in GetProperties(source.GetType()))
			{
				if (info.CanWrite)
				{
					info.SetValue(source, info.GetValue(newValue));
				}
			}
			return source;
		}

		public static T CloneR<T>(this T source) where T : class
		{
			if (source == null)
			{
				return default(T);
			}

			var result = Activator.CreateInstance<T>();

			foreach (var info in typeof (T).GetProperties())
			{
				info.SetValue(result, info.GetValue(source));
			}

			return result;
		}

		public static T CloneJ<T>(this T source) where T : class
		{
			if (source == null)
			{
				return default(T);
			}

			var json = JsonConvert.SerializeObject(source);
			var result = JsonConvert.DeserializeObject<T>(json);

			return result;
		}


		public static string GetCsTypeName(Type type)
		{
			string result;
			using (var provider = new CSharpCodeProvider())
			{
				var typeRef = new CodeTypeReference(type);
				result = provider.GetTypeOutput(typeRef);
			}

			if (result.StartsWith("System.Nullable<") && result.EndsWith(">"))
			{
				result = result.Substring(16, result.Length - 17) + "?";
			}
			var dotIndex = result.IndexOf('.');
			if (dotIndex > 0)
			{
				result = result.Substring(dotIndex + 1);
			}

			return result;
		}

		public static Type GetTypeFromSimpleName(string typeName)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}

			bool isArray = false, isNullable = false;

			if (typeName.IndexOf("[]", StringComparison.Ordinal) != -1)
			{
				isArray = true;
				typeName = typeName.Remove(typeName.IndexOf("[]", StringComparison.Ordinal), 2);
			}

			if (typeName.IndexOf("?", StringComparison.Ordinal) != -1)
			{
				isNullable = true;
				typeName = typeName.Remove(typeName.IndexOf("?", StringComparison.Ordinal), 1);
			}

			typeName = typeName.ToLower();

			string parsedTypeName = null;
			switch (typeName)
			{
				case "bool":
				case "boolean":
					parsedTypeName = "System.Boolean";
					break;
				case "byte":
					parsedTypeName = "System.Byte";
					break;
				case "char":
					parsedTypeName = "System.Char";
					break;
				case "datetime":
					parsedTypeName = "System.DateTime";
					break;
				case "datetimeoffset":
					parsedTypeName = "System.DateTimeOffset";
					break;
				case "decimal":
					parsedTypeName = "System.Decimal";
					break;
				case "double":
					parsedTypeName = "System.Double";
					break;
				case "float":
					parsedTypeName = "System.Single";
					break;
				case "int16":
				case "short":
					parsedTypeName = "System.Int16";
					break;
				case "int32":
				case "int":
					parsedTypeName = "System.Int32";
					break;
				case "int64":
				case "long":
					parsedTypeName = "System.Int64";
					break;
				case "object":
					parsedTypeName = "System.Object";
					break;
				case "sbyte":
					parsedTypeName = "System.SByte";
					break;
				case "string":
					parsedTypeName = "System.String";
					break;
				case "timespan":
					parsedTypeName = "System.TimeSpan";
					break;
				case "uint16":
				case "ushort":
					parsedTypeName = "System.UInt16";
					break;
				case "uint32":
				case "uint":
					parsedTypeName = "System.UInt32";
					break;
				case "uint64":
				case "ulong":
					parsedTypeName = "System.UInt64";
					break;
			}

			if (parsedTypeName != null)
			{
				if (isArray)
				{
					parsedTypeName = parsedTypeName + "[]";
				}

				if (isNullable)
				{
					parsedTypeName = String.Concat("System.Nullable`1[", parsedTypeName, "]");
				}
			}
			else
			{
				parsedTypeName = typeName;
			}

			return Type.GetType(parsedTypeName);
		}
	}
}