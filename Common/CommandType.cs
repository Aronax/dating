namespace Dating.Common
{
	public enum CommandType
	{
		Request = 0,
		Response = 1,
		Event = 2
	}
}