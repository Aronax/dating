﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Dating.Transport
{
	public abstract class NetworkBase
	{
		public delegate void DelConnectionStatusChanged(EventTcpClient sender,
		                                                ConnectionStatus status);

		protected readonly ConcurrentDictionary<Guid, EventTcpClient> _clients = new ConcurrentDictionary<Guid, EventTcpClient>();
		private readonly ConcurrentQueue<RawMessage> _dataQueue = new ConcurrentQueue<RawMessage>();
		private readonly Task _processTask;
		private readonly ConcurrentQueue<RawMessage> _sendQueue = new ConcurrentQueue<RawMessage>();
		private readonly Task _sendTask;
		protected readonly Guid Id = Guid.NewGuid();
		public List<Task> _tasks = new List<Task>();
		protected Action<Exception> ErrorHandler;

		protected NetworkBase()
		{
			_tasks.Add(new Task(SendProcessor));
			_tasks.Add(new Task(ProcessData));

			for (var i = 0; i < 1; i++) {}

			foreach (var task in _tasks)
			{
				task.Start();
			}

			while (_tasks.Any(task => task.Status != TaskStatus.Running))
			{
				Thread.Sleep(1);
			}
		}

		public event DelConnectionStatusChanged ConnectionStatusChanged;
		public event EventHandler<RawMessage> OnDataReceived;

		protected virtual void RaseDataReceived(RawMessage e)
		{
			Console.WriteLine(e.Data);
			var handler = OnDataReceived;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected void eventClient_ConnectionStatusChanged(EventTcpClient sender, ConnectionStatus status)
		{
			if (ConnectionStatusChanged != null)
			{
				ConnectionStatusChanged(sender, status);
			}
		}

		private void SendProcessor()
		{
			while (true)
			{
				RawMessage message;
				while (_sendQueue.TryDequeue(out message))
				{
					try
					{
						if (string.IsNullOrWhiteSpace(message.Data))
						{
							continue;
						}

						EventTcpClient client;
						if (!_clients.TryGetValue(message.ClientId, out client))
						{
							continue;
						}

						if (client.ConnectionState != ConnectionStatus.Connected)
						{
							continue;
						}

						client.Send(message.Data + "\r\n\r\n");
					}
					catch (Exception ex)
					{
						ErrorHandler(ex);
					}
				}
				Thread.Sleep(1);
			}
			// ReSharper disable once FunctionNeverReturns
		}

		public void Send(string message, Guid clientId)
		{
			try
			{
				_sendQueue.Enqueue(new RawMessage(clientId, message));
			}
			catch (Exception ex)
			{
				ErrorHandler(ex);
			}
		}

		public void Send(string message, EventTcpClient client)
		{
			try
			{
				_sendQueue.Enqueue(new RawMessage(client.Id, message));
			}
			catch (Exception ex)
			{
				ErrorHandler(ex);
			}
		}

		protected void DataReceived(EventTcpClient sender, object data)
		{
			try
			{
				var message = data as string;
				_dataQueue.Enqueue(new RawMessage(sender.Id, message));
			}
			catch (Exception ex)
			{
				ErrorHandler(ex);
			}
		}

		public void ProcessData()
		{
			while (true)
			{
				Thread.Sleep(1);
				try
				{
					RawMessage rawMessage;
					while (_dataQueue.TryDequeue(out rawMessage))
					{
						var str = rawMessage.Data;
						EventTcpClient client;
						if (!_clients.TryGetValue(rawMessage.ClientId, out client))
						{
							continue;
						}

						var sb = client.IncomingStringBuilder;
						var endOfMessage = str.EndsWith("\r\n\r\n");
						var completed = string.Empty;
						if (endOfMessage)
						{
							sb.Append(str);
							completed = sb.ToString();
							sb.Clear();
						}
						else
						{
							var ind = str.LastIndexOf("\r\n\r\n", StringComparison.Ordinal);
							if (ind > -1)
							{
								sb.Append(str.Substring(0, ind));
								completed = sb.ToString();
								sb.Clear();
								sb.Append(str.Substring(ind));
							}
							else
							{
								sb.Append(str);
							}
						}

						if (string.IsNullOrWhiteSpace(completed))
						{
							continue;
						}

						var messages = str.Split(new[]
						{
							"\r\n\r\n"
						}, StringSplitOptions.RemoveEmptyEntries);

						foreach (var message in messages)
						{
							RaseDataReceived(new RawMessage(client.Id, message));
						}
					}
				}
				catch (Exception ex)
				{
					ErrorHandler(ex);
				}
			}
			// ReSharper disable once FunctionNeverReturns
		}


		protected void ProcessRequest(string stringRequest, EventTcpClient client)
		{
			try
			{
				//var jCommand = JsonConvert.DeserializeObject<CommandJson>(stringRequest);

				//jCommand.Sender = client;
				//CommandProcessor.Process(jCommand);
			}
			catch (Exception ex)
			{
				ErrorHandler(ex);
			}
		}
	}

	public struct RawMessage
	{
		public RawMessage(Guid clientId, string message) : this()
		{
			ClientId = clientId;

			Data = string.IsNullOrWhiteSpace(message)
				       ? string.Empty
				       : message;
		}

		public Guid ClientId { get; set; }
		public string Data { get; set; }
	}
}