﻿using System;
using System.Net;

namespace Dating.Transport
{
	public class SocketClient : NetworkBase, IDisposable
	{
		private readonly string _address = "127.0.0.1";
		private readonly int _port = 5555;

		private EventTcpClient _eventClient;

		public SocketClient(string address, int port, Action<Exception> errorHandler)
		{
			_port = port;
			_address = address;

			ErrorHandler = errorHandler;
			//CommandProcessor = new CommandProcessor(ErrorHandler);
		}

		public void Dispose()
		{
			_eventClient.Dispose();
		}

		public void Start()
		{
			_eventClient = new EventTcpClient(IPAddress.Parse(_address), _port, ErrorHandler);
			_clients[_eventClient.Id] = _eventClient;
			_eventClient.DataReceived += DataReceived;
			_eventClient.ConnectionStatusChanged += eventClient_ConnectionStatusChanged;
			_eventClient.Connect();
		}

		//public void Send<T>(T request) where T : RequestBase
		//{
		//	Send(request, _eventClient);
		//}

		public void Send(string message)
		{
			Send(message, _eventClient);
		}
	}
}