﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Dating.Transport
{
	public class SocketServer : NetworkBase
	{
		private readonly AutoResetEvent _connectionWaitHandle = new AutoResetEvent(false);
		private readonly int _port = 5555;
		
		public SocketServer(int port, Action<Exception> errorHandler)
		{
			_port = port;
			ErrorHandler = errorHandler;
			//CommandProcessor = new CommandProcessor(ErrorHandler);
		}

		public void Start()
		{
			var listener = new TcpListener(IPAddress.Any, _port);
			listener.Start();

			while (true)
			{
				try
				{
					var result = listener.BeginAcceptTcpClient(HandleAsyncConnection, listener);

					_connectionWaitHandle.WaitOne();
					_connectionWaitHandle.Reset();
				}
				catch (Exception ex)
				{
					ErrorHandler(ex);
				}
			}
		}

		private void HandleAsyncConnection(IAsyncResult result)
		{
			try
			{
				var listener = (TcpListener)result.AsyncState;
				var client = listener.EndAcceptTcpClient(result);
				var ip = ((IPEndPoint)client.Client.RemoteEndPoint).Address;
				var port = ((IPEndPoint)client.Client.RemoteEndPoint).Port;

				_connectionWaitHandle.Set();
				
				var eventClient = new EventTcpClient(ip, port, client, ErrorHandler);
				_clients[eventClient.Id] = eventClient;
				eventClient.DataReceived += DataReceived;
				eventClient.ConnectionStatusChanged += eventClient_ConnectionStatusChanged;
			}
			catch (Exception ex)
			{
				ErrorHandler(ex);
			}
		}
	}
}