﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Timers;
using Timer = System.Timers.Timer;

namespace Dating.Transport
{
	public class EventTcpClient : IDisposable
	{
		public delegate void DelConnectionStatusChanged(EventTcpClient sender,
																		ConnectionStatus status);

		public delegate void DelDataReceived(EventTcpClient sender,
														 object data);

		public StringBuilder IncomingStringBuilder = new StringBuilder();
		private const int Defaulttimeout = 5000;
		private const int Reconnectinterval = 1000;
		private readonly TcpClient _client;
		private readonly byte[] _dataBuffer = new byte[8192];
		private readonly Action<Exception> _errorHandler;
		private readonly IPAddress _ip = IPAddress.None;
		private readonly int _port;
		private readonly object _syncLock = new object();
		private readonly Timer _tmrConnectTimeout = new Timer();

		//Timer used to detect receive timeouts
		private readonly Timer _tmrReceiveTimeout = new Timer();
		private readonly Timer _tmrSendTimeout = new Timer();
		private ConnectionStatus _conStat;
		private Encoding _encode = Encoding.UTF8;
		private Guid _id = Guid.Empty;
		public Guid Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public EventTcpClient(IPAddress ip,
											 int port,
											 Action<Exception> errorHandler,
											 bool autoreconnect = true)
		{
			Id = Guid.NewGuid();
			_ip = ip;
			_port = port;
			AutoReconnect = autoreconnect;
			_client = new TcpClient(AddressFamily.InterNetwork)
			{
				NoDelay = true
			};
			ReceiveTimeout = Defaulttimeout;
			SendTimeout = Defaulttimeout;
			ConnectTimeout = Defaulttimeout;
			ReconnectInterval = Reconnectinterval;
			_tmrReceiveTimeout.AutoReset = false;
			_tmrReceiveTimeout.Elapsed += tmrReceiveTimeout_Elapsed;
			_tmrConnectTimeout.AutoReset = false;
			_tmrConnectTimeout.Elapsed += tmrConnectTimeout_Elapsed;
			_tmrSendTimeout.AutoReset = false;
			_tmrSendTimeout.Elapsed += tmrSendTimeout_Elapsed;
			_errorHandler = errorHandler;

			ConnectionState = ConnectionStatus.NeverConnected;
		}

		public EventTcpClient(IPAddress ip,
											 int port,
											 TcpClient client,
											 Action<Exception> errorHandler)
		{
			Id = Guid.NewGuid();
			_ip = ip;
			_port = port;
			AutoReconnect = false;
			_client = client;
			ReceiveTimeout = Defaulttimeout;
			SendTimeout = Defaulttimeout;
			ConnectTimeout = Defaulttimeout;
			ReconnectInterval = Reconnectinterval;
			_tmrReceiveTimeout.AutoReset = false;
			_tmrReceiveTimeout.Elapsed += tmrReceiveTimeout_Elapsed;
			_tmrConnectTimeout.AutoReset = false;
			_tmrConnectTimeout.Elapsed += tmrConnectTimeout_Elapsed;
			_tmrSendTimeout.AutoReset = false;
			_tmrSendTimeout.Elapsed += tmrSendTimeout_Elapsed;
			_errorHandler = errorHandler;

			ConnectionState = ConnectionStatus.NeverConnected;

			for (var i = 0; i < 20; i++)
			{
				if (client.Connected)
				{
					break;
				}
				Thread.Sleep(50);
			}
			CbConnectComplete();
		}

		public object SyncLock
		{
			get { return _syncLock; }
		}

		public Encoding DataEncoding
		{
			get { return _encode; }
			set { _encode = value; }
		}

		public ConnectionStatus ConnectionState
		{
			get { return _conStat; }
			private set
			{
				var raiseEvent = value != _conStat;
				_conStat = value;

				try
				{
					if (ConnectionStatusChanged != null && raiseEvent)
					{
						ConnectionStatusChanged.BeginInvoke(this, _conStat, CbChangeConnectionStateComplete, this);
					}
				}
				catch (Exception exception)
				{
					_errorHandler(exception);
				}
			}
		}

		public bool AutoReconnect { get; set; }

		public Guid UserId { get; set; }

		public int ReconnectInterval { get; set; }

		public IPAddress Ip
		{
			get { return _ip; }
		}

		public int Port
		{
			get { return _port; }
		}

		public int ReceiveTimeout
		{
			get { return (int)_tmrReceiveTimeout.Interval; }
			set { _tmrReceiveTimeout.Interval = value; }
		}

		public int SendTimeout
		{
			get { return (int)_tmrSendTimeout.Interval; }
			set { _tmrSendTimeout.Interval = value; }
		}


		public int ConnectTimeout
		{
			get { return (int)_tmrConnectTimeout.Interval; }
			set { _tmrConnectTimeout.Interval = value; }
		}

		public void Dispose()
		{
			_tmrConnectTimeout.Dispose();
			_tmrReceiveTimeout.Dispose();
			_tmrSendTimeout.Dispose();
			_client.Close();
		}

		public event DelDataReceived DataReceived;
		public event DelConnectionStatusChanged ConnectionStatusChanged;

		private void tmrSendTimeout_Elapsed(object sender,
														ElapsedEventArgs e)
		{
			ConnectionState = ConnectionStatus.SendFailTimeout;
			DisconnectByHost();
		}

		private void tmrReceiveTimeout_Elapsed(object sender,
															ElapsedEventArgs e)
		{
			ConnectionState = ConnectionStatus.ReceiveFailTimeout;
			DisconnectByHost();
		}

		private void tmrConnectTimeout_Elapsed(object sender,
															ElapsedEventArgs e)
		{
			ConnectionState = ConnectionStatus.ConnectFailTimeout;
			DisconnectByHost();
		}

		private void DisconnectByHost()
		{
			ConnectionState = ConnectionStatus.DisconnectedByHost;
			_tmrReceiveTimeout.Stop();
			if (AutoReconnect)
			{
				Reconnect();
			}
		}

		private void Reconnect()
		{
			if (ConnectionState == ConnectionStatus.Connected)
			{
				return;
			}
			ConnectionState = ConnectionStatus.AutoReconnecting;
			try
			{
				_client.Client.BeginDisconnect(true, CbDisconnectByHostComplete, _client.Client);
			}
			catch (Exception ex)
			{
				// _errorHandler(ex);
			}
		}

		public void Connect()
		{
			try
			{
				if (ConnectionState == ConnectionStatus.Connected)
				{
					return;
				}
				ConnectionState = ConnectionStatus.Connecting;
				_tmrConnectTimeout.Start();
				_client.BeginConnect(_ip, _port, CbConnect, _client.Client);
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}


		public void Disconnect()
		{
			try
			{
				if (ConnectionState != ConnectionStatus.Connected)
				{
					return;
				}
				_client.Client.BeginDisconnect(true, CbDisconnectComplete, _client.Client);
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		public void Send(string data)
		{
			try
			{
				if (ConnectionState != ConnectionStatus.Connected)
				{
					ConnectionState = ConnectionStatus.SendFailNotConnected;
					return;
				}
				var bytes = _encode.GetBytes(data);
				SocketError err;

			//	if (_tmrSendTimeout == null)
			//	{
					
//}
			//	else
	//			{
				//	_tmrSendTimeout.Start();	
//}

				
				_client.Client.BeginSend(bytes, 0, bytes.Length, SocketFlags.None, out err, CbSendComplete,
												 _client.Client);
				if (err != SocketError.Success)
				{
					DisconnectByHost();
				}
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		public void Send(byte[] data)
		{
			try
			{
				if (ConnectionState != ConnectionStatus.Connected)
				{
					throw new InvalidOperationException("Cannot send data, socket is not connected");
				}
				SocketError err;
				_client.Client.BeginSend(data, 0, data.Length, SocketFlags.None, out err, CbSendComplete, _client.Client);
				if (err != SocketError.Success)
				{
					DisconnectByHost();
				}
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbConnectComplete()
		{
			try
			{
				if (_client.Connected)
				{
					_tmrConnectTimeout.Stop();
					ConnectionState = ConnectionStatus.Connected;
					_client.Client.BeginReceive(_dataBuffer, 0, _dataBuffer.Length, SocketFlags.None, CbDataReceived,
														 _client.Client);
				}
				else
				{
					ConnectionState = ConnectionStatus.Error;
				}
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbDisconnectByHostComplete(IAsyncResult result)
		{
			try
			{
				var r = result.AsyncState as Socket;
				if (r == null)
				{
					throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
				}
				r.EndDisconnect(result);
				if (AutoReconnect)
				{
					Connect();
				}
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbDisconnectComplete(IAsyncResult result)
		{
			try
			{
				var r = result.AsyncState as Socket;

				if (r == null)
				{
					throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
				}

				r.EndDisconnect(result);
				ConnectionState = ConnectionStatus.DisconnectedByUser;
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbConnect(IAsyncResult result)
		{
			try
			{
				var sock = result.AsyncState as Socket;

				if (sock != null && !sock.Connected)
				{
					if (!AutoReconnect)
					{
						return;
					}

					Thread.Sleep(ReconnectInterval);
					Connect();
					return;
				}
				if (sock != null)
				{
					sock.EndConnect(result);
				}
				CbConnectComplete();
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbSendComplete(IAsyncResult result)
		{
			try
			{
				var r = result.AsyncState as Socket;
				if (r == null)
				{
					throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
				}
				SocketError err;
				r.EndSend(result, out err);
				if (err != SocketError.Success)
				{
					DisconnectByHost();
				}
				else
				{
					lock (SyncLock)
					{
						_tmrSendTimeout.Stop();
					}
				}
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbChangeConnectionStateComplete(IAsyncResult result)
		{
			try
			{
				var r = result.AsyncState as EventTcpClient;

				if (r == null)
				{
					throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a EDTC object");
				}

				r.ConnectionStatusChanged.EndInvoke(result);
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbDataReceived(IAsyncResult result)
		{
			try
			{
				var sock = result.AsyncState as Socket;
				if (sock == null)
				{
					throw new InvalidOperationException("Invalid IASyncResult - Could not interpret as a socket");
				}

				SocketError err;
				var bytes = sock.EndReceive(result, out err);


				if (bytes == 0 || err != SocketError.Success)
				{
					lock (SyncLock)
					{
						_tmrReceiveTimeout.Start();
						return;
					}
				}
				lock (SyncLock)
				{
					_tmrReceiveTimeout.Stop();
				}
				if (DataReceived == null)
					return;
				DataReceived(this, _encode.GetString(_dataBuffer, 0, bytes));

				_client.Client.BeginReceive(_dataBuffer, 0, _dataBuffer.Length, SocketFlags.None, out err,
				                            CbDataReceived, _client.Client);
				if (err != SocketError.Success)
				{
					DisconnectByHost();
				}
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}

		private void CbDataRecievedCallbackComplete(IAsyncResult result)
		{
			try
			{
				var r = result.AsyncState as EventTcpClient;

				if (r == null)
				{
					throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as EDTC object");
				}
				r.DataReceived.EndInvoke(result);
				SocketError err;
				_client.Client.BeginReceive(_dataBuffer, 0, _dataBuffer.Length, SocketFlags.None, out err,
													 CbDataReceived, _client.Client);
				if (err != SocketError.Success)
				{
					DisconnectByHost();
				}
			}
			catch (Exception exception)
			{
				_errorHandler(exception);
			}
		}
	}

	public enum ConnectionStatus
	{
		NeverConnected,
		Connecting,
		Connected,
		AutoReconnecting,
		DisconnectedByUser,
		DisconnectedByHost,
		ConnectFailTimeout,
		ReceiveFailTimeout,
		SendFailTimeout,
		SendFailNotConnected,
		Error
	}

}
