﻿using System;
using System.Linq;
using Dating.Server.Commands;
using Dating.Server.HandlerEngine;

namespace Dating.Server.Handlers
{
	public class GetRoomsHandler : BaseHandler<GetRoomsRequest>
	{
		public override void Handle(GetRoomsRequest com, App app)
		{
			Console.WriteLine("GetRoomsHandler : {0}", com.Number);
			var response = new GetRoomsResponse
			{
				Rooms = app.Rooms.ToList()
			};

			app.Send(response, com.ClientId);
		}


		
	}

	
}