﻿using System;
using Dating.Server.Commands;
using Dating.Server.HandlerEngine;

namespace Dating.Server.Handlers
{
	public class BaySomeFrenHandler : BaseHandler<BaySomeFrenRequest>
	{
		private Action<BaySomeFrenRequest, App> _handler;


		private readonly object _handlerUpdateLocker = new object();
		public Action<BaySomeFrenRequest, App> Handler {
			get
			{
				lock (_handlerUpdateLocker)
				{
					return _handler;
				}
			}
			set
			{
				lock (_handlerUpdateLocker)
				{
					_handler = value;
				}
			}
		}

		public BaySomeFrenHandler()
		{
			_handler = Realization1;
		}


		public override void Handle(BaySomeFrenRequest com, App app)
		{
			Handler(com, app);
		}

		private void Realization1(BaySomeFrenRequest com, App app)
		{
			
		}

	}
}