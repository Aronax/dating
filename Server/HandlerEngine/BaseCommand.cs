﻿using System;
using System.Collections.Concurrent;
using System.IO;
using Dating.Transport;
using Newtonsoft.Json;

namespace Dating.Server.HandlerEngine
{
	public class BaseCommand 
	{
		private static readonly ConcurrentDictionary<string, Type> _allCommands = new ConcurrentDictionary<string, Type>();

		private static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
		{
			ContractResolver = new OrderedContractResolver()
		};

		static BaseCommand()
		{
			foreach (var item in HandlerDefenition.GetAllInheritance<BaseCommand>())
			{
				_allCommands.TryAdd(item.Name.ToLower(), item);
			}
		}

		public BaseCommand()
		{
			Name = GetType().Name;
		}

		[JsonProperty(Order = 0)]
		public string Name { get; set; }

		[JsonProperty("ActionId", NullValueHandling = NullValueHandling.Ignore)]
		public string ActionId { get; set; }

		public CommandType Type { get; set; }

		[JsonIgnore]
		public Guid ClientId { get; set; }


		public static string GetCommandNameFromJson(string str)
		{
			try
			{
				using (var sr = new StringReader(str))
				{
					using (var reader = new JsonTextReader(sr))
					{
						while (reader.Read())
						{
							if (reader.Value != null && reader.Value.Equals("Name"))
								return reader.ReadAsString();
						}
						reader.Close();
					}
				}
				return string.Empty;
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		public static BaseCommand FromJson(RawMessage message)
		{
			var result = FromJson(message.Data);
			if (result != null)
				result.ClientId = message.ClientId;

			return result;
		}

		public static BaseCommand FromJson(string str)
		{
			var commandName = GetCommandNameFromJson(str);
			if (string.IsNullOrWhiteSpace(commandName))
				return null;

			Type commandType;
			if (!_allCommands.TryGetValue(commandName.ToLower(), out commandType))
				return null;

			var command = JsonConvert.DeserializeObject(str, commandType) as BaseCommand;
			return command;
		}

		public string ToJson()
		{
			return ToJson(this);
		}

		public static string ToJson(BaseCommand command)
		{
			var json = JsonConvert.SerializeObject(command, Formatting.None, _jsonSerializerSettings);
			return json;
		}
	}
}