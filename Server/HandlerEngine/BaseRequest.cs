﻿namespace Dating.Server.HandlerEngine
{
	public abstract class BaseRequest : BaseCommand
	{
		protected BaseRequest(){
			Type = CommandType.Request;
		}
	}
}