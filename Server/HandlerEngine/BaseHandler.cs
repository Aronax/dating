﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dating.Server.HandlerEngine
{
	public interface IBaseHandler {}

	public abstract class BaseHandler<T> : IBaseHandler where T : BaseCommand
	{
		public abstract void Handle(T com, App app);
	}

	public enum InstanceMode
	{
		Single = 0,
		PerCall = 1
	}

	public class HandlerDefenition
	{
		public HandlerDefenition(Type handlerType, Type commandType, object handlerInstance, MethodInfo hadlerMethod, InstanceMode instanceMode)
		{
			HandlerType = handlerType;
			CommandType = commandType;
			HandlerInstance = handlerInstance;
			HadlerMethod = hadlerMethod;
			InstanceMode = instanceMode;
		}

		public Type HandlerType { get; set; }
		public Type CommandType { get; set; }
		public object HandlerInstance { get; set; }
		public MethodInfo HadlerMethod { get; set; }
		public InstanceMode InstanceMode { get; set; }

		public static IEnumerable<Type> GetAllImplementations<T>()
		{
			var type = typeof (T);
			var typeList = Assembly.GetAssembly(type)
			                       .GetTypes()
			                       .Where(t => t.GetInterface(type.Name) != null && t.IsAbstract == false)
			                       .ToList();
			return typeList;
		}

		public static List<Type> GetAllInheritance<T>() where T : class
		{
			var type = typeof (T);
			var typeList = Assembly.GetAssembly(typeof (T)).
			                        GetTypes().
			                        Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(type)).
			                        ToList();
			return typeList;
		}

		public static ConcurrentDictionary<Type, List<HandlerDefenition>> GetMessageHandlerDefenitions()
		{
			var result = new ConcurrentDictionary<Type, List<HandlerDefenition>>();
			var types = GetAllImplementations<IBaseHandler>();

			foreach (var handlerType in types)
			{
				var mode = InstanceMode.Single;
				var attrib = handlerType.GetCustomAttribute<InstanceModeAttribute>();

				if (attrib != null)
					mode = attrib.InstanceMode;

				var baseType = handlerType.BaseType;
				if (baseType == null || !baseType.IsGenericType)
					continue;

				var dataType = baseType.GetGenericArguments().
				                        FirstOrDefault();
				if (dataType == null)
					continue;

				if (!result.ContainsKey(dataType))
					result[dataType] = new List<HandlerDefenition>();

				result[dataType]
					.Add(new HandlerDefenition(handlerType,
					                           dataType,
					                           mode == InstanceMode.Single
						                           ? Activator.CreateInstance(handlerType)
						                           : null,
					                           handlerType.GetMethod("Handle"),
					                           mode));
			}
			return result;
		}
	}

	[AttributeUsage(AttributeTargets.Class)]
	public class InstanceModeAttribute : Attribute
	{
		public InstanceModeAttribute(InstanceMode mode = InstanceMode.Single)
		{
			InstanceMode = mode;
		}


		public InstanceMode InstanceMode { get; private set; }
	}
}