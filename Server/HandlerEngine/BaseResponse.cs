﻿namespace Dating.Server.HandlerEngine
{
	public abstract class BaseResponse : BaseCommand
	{
		protected BaseResponse()
		{
			Type = CommandType.Response;
		}
	}
}