﻿using Dating.Server.HandlerEngine;

namespace Dating.Server.Commands
{
	public class BaySomeFrenRequest : BaseRequest
	{
		public string FrenName { get; set; }
	}
}
