﻿using System.Collections.Generic;
using Dating.Server.Entities;
using Dating.Server.HandlerEngine;

namespace Dating.Server.Commands
{
	public class GetRoomsResponse : BaseResponse
	{
		public GetRoomsResponse()
		{
			Rooms = new List<Room>();
		}

		public List<Room> Rooms { get; set; }
	}
}
