﻿using Dating.Server.HandlerEngine;

namespace Dating.Server.Commands
{
	public class GetRoomsRequest : BaseRequest
	{
		public GetRoomsRequest() {}
		public int Number { get; set; }
	}

	public class PingRequest : BaseRequest {
		public string Message { get; set; }
	}
	
	public class PongResponse : BaseResponse {
		public string SomeResponseText { get; set; }
	}

	public class PingHandler : BaseHandler<PingRequest> {
		public override void Handle(PingRequest com, App app) {

			var response = new PongResponse {
				SomeResponseText = com.Message + " response !!!"
			};
			app.Send(response, com.ClientId);
		}
	}

}
