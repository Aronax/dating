﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.Linq;
using MongoDB.Driver;

namespace Dating.Server
{
	public interface IDataAccess
	{
		string ConnectionString { get; }
		T Save<T>(T obj);
		T Insert<T>(T obj);
		IEnumerable<T> InsertBatch<T>(IEnumerable<T> list);
		IList<T> InsertBatch<T>(IList<T> list);
		IList<T> LoadAll<T>();
	}

	public class MongoDataAccess : IDataAccess
	{
		private static readonly PluralizationService PluralizationService;

		private readonly ConcurrentDictionary<Type, MongoCollection> _collections = new ConcurrentDictionary<Type, MongoCollection>();
		private readonly MongoDatabase _mongoDatabase;
		private MongoClient _mongoClient;
		private MongoServer _mongoServer;

		static MongoDataAccess()
		{
			PluralizationService = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en-us"));
		}

		public MongoDataAccess(string connectionString)
		{
			ConnectionString = connectionString;

			_mongoClient = new MongoClient(connectionString);
			_mongoServer = _mongoClient.GetServer();
			_mongoDatabase = _mongoServer.GetDatabase("dating");
		}

		public string ConnectionString { get; private set; }

		public T Save<T>(T obj)
		{
			var result = GetCollection<T>().
				Save(obj);
			return obj;
		}

		public T Insert<T>(T obj)
		{
			var result = GetCollection<T>().
				Insert(obj);
			return obj;
		}

		public IEnumerable<T> InsertBatch<T>(IEnumerable<T> list)
		{
			var result = GetCollection<T>().
				InsertBatch(list);
			return list;
		}

		public IList<T> InsertBatch<T>(IList<T> list)
		{
			var result = GetCollection<T>().
				InsertBatch(list);
			return list;
		}


		public IList<T> LoadAll<T>()
		{
			return GetCollection<T>().
				FindAll().
				ToList();
		}

		public static string GetTableNameByType(string name)
		{
			return PluralizationService.Pluralize(name);
		}

		private MongoCollection<T> GetCollection<T>()
		{
			var type = typeof (T);

			MongoCollection collection;
			if (_collections.TryGetValue(type, out collection))
			{
				return collection as MongoCollection<T>;
			}
			var result = _mongoDatabase.GetCollection<T>(GetTableNameByType(type.Name));
			result = _collections.AddOrUpdate(type, result, (t, col) => col) as MongoCollection<T>;
			return result;
		}
	}
}