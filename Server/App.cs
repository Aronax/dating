﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Dating.Server.Entities;
using Dating.Server.HandlerEngine;
using Dating.Transport;

namespace Dating.Server
{
	public class App
	{
		private static readonly ConcurrentDictionary<Type, List<HandlerDefenition>> _handlerDefenitions = new ConcurrentDictionary<Type, List<HandlerDefenition>>();
		private readonly ConcurrentQueue<RawMessage> _inMessages = new ConcurrentQueue<RawMessage>();
		private readonly SocketServer _socketServer;
		private readonly List<Task> _tasks = new List<Task>();

		static App()
		{
			_handlerDefenitions = HandlerDefenition.GetMessageHandlerDefenitions();
		}

		public App(IDataAccess dataAccess, int port, Action<Exception> errorHandler = null)
		{
			ErrorHandler = errorHandler ?? Console.WriteLine;

			try
			{
				//connections and threads 
				ServicePointManager.DefaultConnectionLimit = 5000;
				ThreadPool.SetMinThreads(64, 128);
				ThreadPool.SetMaxThreads(128, 256);

				Concurrent = Concurrent <= 0
					             ? Environment.ProcessorCount
					             : Concurrent;

				//Transport
				_socketServer = new SocketServer(port, errorHandler);
				_tasks.Add(new Task(_socketServer.Start));
				_socketServer.OnDataReceived += _socketServer_OnDataReceived;
				for (var i = 0; i < Concurrent; i++)
				{
					_tasks.Add(new Task(ProcessMessage));
				}

				//DataAccess
				DataAccess = dataAccess;

				//Collections
				Rooms = new RoomCollection(DataAccess);
			}
			catch (Exception ex)
			{
				ErrorHandler(ex);
			}
		}

		public Action<Exception> ErrorHandler { get; set; }
		public IDataAccess DataAccess { get; private set; }
		public int Concurrent { get; set; }
		public RoomCollection Rooms { get; private set; }

		public void Send(BaseCommand command, Guid clientId)
		{
			_socketServer.Send(command.ToJson(), clientId);
		}

		private bool HandleMessage(RawMessage message)
		{
			var command = BaseCommand.FromJson(message);
			if (command == null)
			{
				return false;
			}

			List<HandlerDefenition> items;
			if (!_handlerDefenitions.TryGetValue(command.GetType(), out items))
			{
				return false;
			}

			foreach (var item in items)
			{
				InvokeHandler(item,command);
			}
			return true;
		}

		private void InvokeHandler(HandlerDefenition handler, BaseCommand command)
		{
			try
			{
				var instance = handler.InstanceMode == InstanceMode.Single
								   ? handler.HandlerInstance
								   : Activator.CreateInstance(handler.HandlerType);

				handler.HadlerMethod.Invoke(instance, new object[] { command, this });
			}
			catch (Exception ex)
			{
				ErrorHandler(ex);
			}
			
		}

		private void _socketServer_OnDataReceived(object sender, RawMessage e)
		{
			_inMessages.Enqueue(e);
		}

		private void ProcessMessage()
		{
			while (true)
			{
				Thread.Sleep(1);
				RawMessage message;
				while (_inMessages.TryDequeue(out message))
				{
					try
					{
						HandleMessage(message);
					}
					catch (Exception ex)
					{
						ErrorHandler(ex);
					}
				}
			}
		}

		public void Run()
		{
			foreach (var task in _tasks)
			{
				task.Start();
			}

			while (_tasks.Any(task => task.Status != TaskStatus.Running))
			{
				Thread.Sleep(1);
			}
		}
	}
}