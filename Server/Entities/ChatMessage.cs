﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dating.Common;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace Dating.Server.Entities
{
	public class ChatMessage
	{
		[JsonConverter(typeof(ObjectIdConverter))]
		public ObjectId Id { get; set; }

		[JsonConverter(typeof(ObjectIdConverter))]
		public ObjectId FromUserId { get; set; }

		[JsonConverter(typeof(ObjectIdConverter))]
		public ObjectId? ToUserId { get; set; }

		[JsonConverter(typeof(ObjectIdConverter))]
		public ObjectId RoomId { get; set; }
		
		public DateTime Created { get; set; }
		public DateTime? Readed { get; set; }
		public bool	IsPrivate { get; set; }
		public bool IsReaded { get; set; }
		public string Text { get; set; }
	}
}
