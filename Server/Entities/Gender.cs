﻿namespace Dating.Server.Entities
{
	public enum Gender
	{
		Female = 0,
		Male = 1
	}
}