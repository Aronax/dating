﻿using System;
using Dating.Common;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace Dating.Server.Entities
{
	public class Room
	{
		[JsonConverter(typeof(ObjectIdConverter))]
		public ObjectId Id { get; set; }
		public string Name { get; set; }
		public RoomType Type { get; set; }

		public int MinTables { get; set; }
		public int MaxTables { get; set; }
		public DateTime Created { get; set; }

		public override string ToString()
		{
			return string.Format("Created: {0}, MaxTables: {1}, MinTables: {2}, Name: {3}, Type: {4}", Created, MaxTables, MinTables, Name, Type);
		}
	}
}
