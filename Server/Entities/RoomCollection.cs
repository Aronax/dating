﻿using System;
using Dating.Common;
using MongoDB.Bson;

namespace Dating.Server.Entities
{
	public class RoomCollection : KeyCollection<ObjectId, Room>
	{
		public IDataAccess DataAccess { get; private set; }

		public RoomCollection(IDataAccess dataAccess) : base("Id")
		{
			DataAccess = dataAccess;
		}
		
		public RoomCollection Load()
		{
			Clear();
			AddRange(DataAccess.LoadAll<Room>());
			return this;
		}

		public Room Create(string name, RoomType roomType, int minTables = 3, int maxTables = 6)
		{
			var room = new Room
			{
				Id = new ObjectId(),
				Name = name,
				Type = roomType,
				MinTables = minTables,
				MaxTables = maxTables,
				Created = DateTime.Now
			};
			return Add(DataAccess.Save(room));
		}
	}
}