﻿using MongoDB.Bson;

namespace Dating.Server.Entities
{
	public class User
	{
		public ObjectId Id { get; set; }
		public string Name { get; set; }
		public Gender Gender { get; set; }
	}
}