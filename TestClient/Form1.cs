﻿using System;
using System.Windows.Forms;
using Dating.Server.Commands;
using Dating.Server.HandlerEngine;
using Dating.Transport;

namespace TestClient
{
	public partial class Form1 : Form
	{
		private SocketClient _client;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			_client = new SocketClient("127.0.0.1", 7777, ex => Do(() => listBox1.Items.Add(ex)));
			_client.Start();

			_client.Send("test");
			_client.ConnectionStatusChanged += _client_ConnectionStatusChanged;

			_client.OnDataReceived += _client_OnDataReceived;
		}

		private void _client_OnDataReceived(object sender, RawMessage e)
		{
			Do(() =>
			   {
				   listBox1.Items.Add(e.Data);
				   var command = BaseCommand.FromJson(e.Data);
				   command.ClientId = e.ClientId;

				   ProcessCommand(command);
			   });
		}

		public void ProcessCommand(GetRoomsResponse command)
		{
			foreach (var room in command.Rooms)
			{
				listBox2.Items.Add(room);
			}
		}

		public void ProcessCommand(BaseCommand command)
		{
			var response = command as GetRoomsResponse;
			if (response != null)
			{
				ProcessCommand(response);
			}
		}

		private void _client_ConnectionStatusChanged(EventTcpClient sender, ConnectionStatus status)
		{
			Do(() => listBox1.Items.Add(status));
		}

		private void button1_Click(object sender, EventArgs e)
		{
			_client.Send(new GetRoomsRequest().ToJson());
		}

		public void Do(Action action)
		{
			try
			{
				Invoke(action);
			}
			catch {}
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			textBox1.Text = Convert.ToString(listBox1.SelectedItem);
		}
	}
}