﻿using System;
using System.Linq;
using Dating.Server;
using Dating.Server.Entities;

namespace ConsoleServer
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var app = new App(new MongoDataAccess("mongodb://localhost"), 7777);
			var rooms = app.Rooms.Load();
			rooms.Create("Tflkjfxkjhxkhjxkhjxljfljfljooomq", RoomType.Global);

			foreach (var room in rooms.OrderBy(n=>n.Created))
			{
				Console.WriteLine(room);
			}

			app.Run();

			Console.WriteLine("For exit press any key...");
			Console.ReadKey();
		}

		public static void Run() {}
	}
}